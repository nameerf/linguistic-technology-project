
import os
import glob
import re
import pickle
#from nltk import clean_html
from unicodedata import normalize

WORKING_DIRECTORY = os.getcwd()

docs = 0
words = 0
nouns = 0 
urls = 0

#===============================================================================
# H synarthsh "import_xml_files" de dexetai kapoio orisma kai exei thn ekshs
# leitourgia: fortwnei ta paths twn arxeiwn xml ths sylloghs mas apo ton fakelo
# wikipedia kai ta apo8hkeyei -mesw ths synarthshs glob toy module glob- sthn
# lista wikipedia_xml_files. Sth synexeia antistoixoyme ena monadiko ari8mo
# (id, gia eykolia xrhsimopoioyme ari8mhsh apo to 0 mexri to 3999) se ka8e path
# kai dhmioyrgoyme ena leksiko me kleidia ta ids twn arxeiwn kai times ta paths
# twn arxeiwn sto dikso. Katopin mesw toy module picle (poy apo8hkeyei
# antikeimena sto disko - serialization) apo8hkeyoyme to leksiko ayto sto disko
# sto arxeio me onoma "texts_ids.txt". Telos h synarthsh epistrefei to leksiko
# poy dhmioyrgh8hke.
#===============================================================================
def import_xml_files():
    global docs
    wikipedia_folder = os.path.join(WORKING_DIRECTORY,'wikipedia')
    wikipedia_xml_files = glob.glob(wikipedia_folder+'\\*.xml')
    docs = len(wikipedia_xml_files)
        
    texts_ids_dict = {}
    for i in range(len(wikipedia_xml_files)):
        texts_ids_dict[i] = wikipedia_xml_files[i]
        
    #print texts_ids_dict
    with open('texts_ids.txt', 'w') as f:
        pickle.dump(texts_ids_dict, f)
    
    return texts_ids_dict

#===============================================================================
# H synarthsh "convert_text_to_tagged_tokens" dexetai dyo orismata: to proto
# einai to "text_id" poy antiprosopeyei to id toy arxeioy kai to deytero einai
# to "xml_file" poy einai to xml arxeio pros (pro-)epeksergasia. H leitoyrgia
# ths synarthshs einai h ekshs: katarxhn anoigoyme to arxeio "xml_file" gia
# diabasma kai apo8hkeyoyme ta periexomena tou sth string metablhth xml. Sth
# synexeia me thn boh8heia twn (poly dynatwn) regular expressions ka8arizoyme
# to arxeio apo ta meta-data kai ola ta mh aparaithta stoixeia (blepe anafora
# gia perissotera), kanoyme tokenization outos oste ka8e token na brisketai se
# ksexoristh grammh kai telos apo8hkeyoyme to apotelesma (poy einai pleon
# apo8hkeymeno sth string metablhth "text") se ena prosorino arxeio me onoma
# "tokenized_file.txt". Katopin kaloyme thn synarthsh "tag_tokens" me orisma to
# id toy arxeioy "text_id". Telos h synarthsh epistrefei to epeksergasmeno
# keimeno "text" os string (xrhsimopoih8hke kyrios gia skopoys debugging).
#===============================================================================
def convert_text_to_tagged_tokens(text_id, xml_file):
    global words
    global urls
    
    #print xml_file
    with open(xml_file, 'r') as f:
        xml = f.read()

    #print 'cleaning and tokenizing text...'
    #text = clean_html(xml)
    
    text = re.sub('<[^>]+>', ' ', xml)
    text = re.sub('/ref&gt;|&amp;', ' ', text)
    (text, local_urls) = re.subn('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ', text)
    urls += local_urls
    text = normalize('NFKD', unicode(text)).encode('ascii', 'ignore')
    text = re.sub('\.|\,|\;|\:|\/|\\|\||\_|\(|\)\{|\}|\[|\]', ' ', text)
    text = re.sub('\d+th|\d+', ' ', text)
    text = re.sub('[^\s\w\'-]', ' ', text)
    text = re.sub('(?<=[a-zA-Z])\'(?=[a-zA-Z]+)', '$', text)
    text = re.sub('(?<=[a-zA-Z])\-(?=[a-zA-Z]+)', '@', text)
    text = re.sub('[^\s\w\$\@]', ' ', text)
    text = re.sub('\$', '\'', text)
    text = re.sub('\@', '-', text)
    text = re.sub('\s.(\s.)*\s', ' ', text)     # 'one-letter' words (includes cons...
#    text2 = text
    splited_text = text.split()
    
    words += len(splited_text)
    
    with open('tokenized_file.txt', 'w') as f:
        #f.write(text)
        for token in splited_text:
            f.write(token.lower()+'\n')     # metatrepoyme to ka8e token se mikra grammata
#    with open('cleaned_file.txt', 'w') as f2:            # for debugging purposes
#        f2.write(text2)
    
    #print 'tagging tokenized text...'
    tag_tokens(text_id)
    #print 'done'
    count_nouns(text_id)
    
    return text

#===============================================================================
# H synarthsh "tag_tokens" dexetai os monadiko orisma to "text_id" to opoio
# antiprosopeyei to id toy arxeioy pros tagging. H leitoyrgia ths einai h ekshs
# : katarxhn ginetai elegxos gia ton ean yparxei o fakelos "tagged_files" -sto
# working directory- kai ean oxi, dhmioyrgeitai. O fakelos aytos einai enas
# prosorinos fakelos ston opoio apo8hkeyontai ta tagged arxeia kai meta thn
# dhmioyrgia twn eyretiriwn diagrafetai. Sth synexeia mesw ths synarthshs popen
# kaloyme thn eksoterikh diergasia tou tagger me orisma to apo8hkeymeno
# tokenized arxeio "tokenized_file.txt" kai afoy o tagger kanei to tagging
# apo8hkeyoyme thn eksodo tou sto prosorino arxeio me onoma 
# "tagged_file_id.txt", opou to id einai to id tou ekastote arxeioy ka8e fora,
# p.x. tagged_file_999.txt. Telos diagrafoume to prosorino arxeio me onoma
# "tokenized_file.txt".
#===============================================================================
def tag_tokens(text_id):
    if not os.path.exists('tagged_files'):
        os.makedirs('tagged_files')
    temp_dir = os.path.join(WORKING_DIRECTORY,'tagged_files')
    filename = temp_dir+'\\tagged_file_'+str(text_id)+'.txt'
    #print filename
    os.popen('tag-english tokenized_file.txt > {0}'.format(filename))
    os.remove('tokenized_file.txt')


#===============================================================================
# H synarthsh "count_nouns" dexetai os monadiko orisma to "text_id" to opoio
# antiprosopeyei to id toy arxeioy pros tagging. H leitoyrgia ths einai apla
# na metrhsei ta ousiastika ka8e arxeioy. Ayto to kanei me to na metraei oles
# tis emfaniseis ton open class categories NN, NNS, NNP, NNPS oi opoies oles
# antiprosopeuoun ousiastika.
#===============================================================================
def count_nouns(text_id):
    global nouns
    
    temp_dir = os.path.join(WORKING_DIRECTORY,'tagged_files')
    filename = temp_dir+'\\tagged_file_'+str(text_id)+'.txt'
    with open(filename, 'r') as f:
        file_as_string = f.read()

    #print file_as_string
    results = re.findall('\tNNS?P?(PS)?\t', file_as_string)
    nouns += len(results)

#===============================================================================
# H synarthsh "export_statistics" den dexetai kapoio orisma. H leitourgia ths
# einai apla na eksagei/apo8hkeusei ta statistika ths sylloghs pou zhtountai
# sto arxeio statistics.txt.
#===============================================================================
def export_statistics():
    global docs
    global words
    global nouns 
    global urls
    
    words_per_document = float(words) / float(docs)
    nouns_per_document = float(nouns) / float(docs)
    urls_per_document = float(urls) / float(docs)
    
    with open('statistics.txt', 'w') as f:
        f.write('Total number of documents in collection = {0}'.format(docs)+'\n')
        f.write('Total number of words in collection = {0}'.format(words)+'\n')
        f.write('Total number of nouns in collection = {0}'.format(nouns)+'\n')
        f.write('Total number of urls in collection = {0}'.format(urls)+'\n')
        f.write('Number of words per document = {0}'.format(words_per_document)+'\n')
        f.write('Number of nouns per document = {0}'.format(nouns_per_document)+'\n')
        f.write('Number of urls per document = {0}'.format(urls_per_document)+'\n')
