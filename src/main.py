import os
import re
import sys
import pickle
import preprocessing
import create_indexes
import save_and_load_indexes
import search_indexes as SE
import compare_indexes as CI

import time

NUMBER_OF_FILES = 0
INVERTED = 1
NORMAL = 2
inverted_index = None
normal_index = None

#===============================================================================
# H "main" einai h kyria synarthsh apo thn opoia ekkinei to programma. Apla
# kalei thn synarthsh "search_engine" kai epistrfei.
#===============================================================================
def main():
    search_engine()
    
    print 'success!'
    return 0

#===============================================================================
# H synarthsh "search_engine" ulopoiei mia aplh mhxanh anazhthshs thn opoia
# xrhsimopoiei o xrhsths gia na 8etei ta erwthmata toy. Pio sygkekrimena h
# leitoyrgia ths einai h ekshs: arxika elegxetai to an exoyn dhmioyrgh8ei ta
# eyrethria kai apo8hkey8ei sto disko. Sthn periptosh poy ayto de symbainei
# kaleitai h synarthsh "create_and_save_indexes" h opoia analambanei na
# dhmioyrghsei kai na apo8hkeysei ta eyrethria sto disko etsi oste na mporei o
# xrhsths na xrhsimopoihsei amesa thn mhxanh anazhthshs. H apo8hkeysh ginetai
# gia tyxon mellontikh xrhsh ths mhxanhs etsi oste na fortw8oyn ta eyrethria
# apo to disko kai na mhn xreiastei na ksanadhmioyrgh8oyn. Sthn periptosh poy
# ta eyrethria einai apo8hkeymena sto disko apla fortwnontai sth mnhmh. Sth
# synexeia, afoy ta eyrethria fortw8oyn sth mnhmh, emfanizetai ena mikro menu
# poy protrepei to xrhsth na dialeksei se poio eyrethrio epi8ymei na ginei h
# anazhthsh kai katopin protrepetai na 8esei to erwthma poy epi8ymei. Yparxei
# kai mia epilogh gia sygkrish ton dyo eyrethrion xrhsimopoiontas ta erothmata
# pou briskontai sto arxeio queries.txt pou dinetai, opos perigrafetai sthn
# ekfwnhsh. To erwthma toy xrhsth metatrepetai sthn katallhlh morfh (afairoyntai
# tyxon stop-words ginetai metatroph se mikra grammata klp.) prin xrhsimopoih8ei.
# Afoy o xrhsths dhlosei to epi8umhto eyrethrio kai dosei to erwthma pros
# anazhthsh kaleitai h antistoixh synarthsh apo tis "search_inverted_index"
# kai "search_normal_index" me orisma to fortwmeno eyrethrio kai to erwthma
# pros anazhthsh toy xrhsth. Telos epistrefontai kai typonontai ta opoia 
# keimena bre8hkan, sthn o8onh, taksinomhmena me bash thn omoiothta toy
# dianysmatos toy ka8e keimenoy me to dianysma toy erwthmatos toy xrhsth h me
# alla logia taksinomhmena apo to perissotero sto ligotero sxetiko.
#===============================================================================
def search_engine():
    global inverted_index
    global normal_index
    
    try:
        print 'Trying to load the indexes, please wait...'
        start_time = time.time()
        (inverted_index, normal_index) = save_and_load_indexes.load_indexes_from_disk()
        with open('texts_ids.txt', 'r') as f:
            xml_files_ids_dict = pickle.load(f)
        print 'time required = {0} sec'.format(time.time()-start_time)
    except IOError:
        print 'Indexes haven\'t been created. Creating and loading them now, please wait...'
        xml_files_ids_dict = create_and_save_indexes()
        #sys.stderr.write('Error: ' + str(e) + '\n')
    
    while True:
        print '\t\tMain menu'
        print '1) Search with the inverted index'
        print '2) Search with the normal index'
        print '3) Compare the two indexes'
        print '4) Exit'
        
        while True:
            option = raw_input('Please enter your choice ==> ')
            if int(option) == 4:
                exit()
            if not str.isdigit(option) or int(option) not in [1, 2, 3, 4]:
                print 'Your choice is invalid, please enter 1, 2, 3 and 4.'
            else:
                break
        if int(option) == 3:
            CI.compare_search_times(inverted_index, normal_index)
            continue
        
        user_input = raw_input('Enter your query ==> ')
        user_input = re.sub('\s+', '\n', user_input)
        #print user_input
        with open('temp_tokenized_file.txt', 'w') as f:
            f.write(user_input)
        
        os.popen('tag-english temp_tokenized_file.txt > temp_tagged_file.txt')
        
        with open('temp_tagged_file.txt', 'r') as f:
            temp_lines = f.readlines()
        
        exit()
        
        query = create_indexes.extract_lemmas(temp_lines)
        os.remove('temp_tokenized_file.txt')
        os.remove('temp_tagged_file.txt')
        #print query
        query = list(set(query))
        #print query
        
        #print final_list
        
        index_type = int(option)
        if index_type == INVERTED:
            results = SE.search_inverted_index(inverted_index, query)
        elif index_type == NORMAL:
            results = SE.search_normal_index(normal_index, query)
            
        if len(results) > 0:
            #print results
            for i in results:
                print xml_files_ids_dict[i]
        else:
            print 'Den bre8hke kapoio keimeno me to erothma pou 8esate'
        print '\n'

#===============================================================================
# H synarthsh "create_and_save_indexes" xrhsimopoieitai apo thn synarthsh
# "search_engine" sthn periptosh poy den exoyn dhmioyrgh8ei ta dyo eyrethria.
# Opote h leitoyrgia ths synarthshs einai na dhmioyrghsei ta dyo eyrethria
# (kalwntas tis antistoixes synarthseis) kai epishs na ta apo8hkeysei sto disko.
#===============================================================================
def create_and_save_indexes():
    global inverted_index
    global normal_index
    xml_files_ids_dict = preprocessing.import_xml_files()
    NUMBER_OF_FILES = preprocessing.docs
    #NUMBER_OF_FILES = 2000
    time_start = time.time()
    for i in range(NUMBER_OF_FILES):
        preprocessing.convert_text_to_tagged_tokens(i, xml_files_ids_dict[i])
    print 'time passed for preprocessing = {0} sec'.format(time.time() - time_start)
    
    preprocessing.export_statistics()
    
    (inverted_index, normal_index) = create_indexes.create(NUMBER_OF_FILES)
    
    save_and_load_indexes.write_indexes_to_disk(inverted_index, normal_index)
    
    return xml_files_ids_dict


if __name__ == '__main__':
    status = main()
    exit(status)
