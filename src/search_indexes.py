#===============================================================================
# H synarthsh "search_inverted_index" dexetai dyo orismata: to "inverted_index"
# to opoio einai to anestrammeno eyrethrio kai to "query" to opoio einai to
# erwthma pros anazhthsh poy e8ese o xrhsths. Sygkekrimena to "query" einai mia
# lista poy periexei toys oroys anazhthshs toy xrhsth. H leitoyrgia ths
# synarthshs einai apla h anazhthsh twn orwn toy xrhsth sto anestrmmeno
# eyrethrio kai h epistrofh twn apotelesmatwn taksinomhmena me bash thn
# omoiothta toy dianysmatos toy ka8e keimenoy me to dianysma toy erwthmatos.
# Gia thn epiteyksh toy teleytaioy kaleitai h synarthsh
# "results_sorted_by_similarity".
#===============================================================================
def search_inverted_index(inverted_index, query):
    #print 'searching in inverted index...'
    #print query
    #print inverted_index.items()
    similarity_dict = dict()
    
    for keyword in query:
        if keyword in inverted_index:
            nested_dict = inverted_index[keyword]
            text_ids = iter(nested_dict)
            for j in text_ids:
                #print 'keyword {0} exists in text with id {1}'.format(keyword, j)
                if j in similarity_dict:
                    similarity_dict[j] += float(nested_dict[j])
                else:
                    similarity_dict[j] = float(nested_dict[j])
                
    #print similarity_dict.items()
    return results_sorted_by_similarity(similarity_dict)

#===============================================================================
# H synarthsh "search_normal_index" dexetai dyo orismata: to "normal_index" to
# opoio einai to kanoniko eyrethrio kai to "query" to opoio einai to erwthma
# poy e8ese o xrhsths pros anazhthsh. Ta ypoloipa einai akribos opws
# perigrafhkan parapanw sth synarthsh "search_inverted_index" me th diafora oti
# edw exoyme to kanoniko eyrethrio anti toy anestrammenoy.
#===============================================================================
def search_normal_index(normal_index, query):
    #print 'searching in normal index...'
    #print query
    #print normal_index.items()
    similarity_dict = dict()
    
    for text_id in range(len(normal_index)):
        nested_dict = normal_index[text_id]
        for keyword in query:
            if keyword in nested_dict:
                #print 'keyword {0} exists in text with id {1}'.format(query, i)
                if text_id in similarity_dict:
                    similarity_dict[text_id] += float(nested_dict[keyword])
                else:
                    similarity_dict[text_id] = float(nested_dict[keyword])
                                                 
    #print similarity_dict.items()
    return results_sorted_by_similarity(similarity_dict)

#===============================================================================
# H synarthsh "results_sorted_by_similarity" dexetai ena orisma, to
# "similarity_dict". To orisma ayto einai ena leksiko sto opoio einai
# apo8hkeymena ta esoterika ginomena twn barwn twn lhmmatwn me toys oroys
# anazhthshs toy xrhsth, opoy ta kleidia einai ta ids twn keimenwn kai oi times
# toy leksikoy einai ta esoterika ginomena. H leitoyrgia ths synarthshs einai na
# epistrepsei ta telika apotelesmata taksinomhmena me to cosine similarity dhl.
# apo to pio sxetiko keimeno pros to ligotero sxetiko. Epistrefei thn lista
# results me ta ids twn keimenwn.
#===============================================================================
def results_sorted_by_similarity(similarity_dict):
    results = []
    while len(similarity_dict) > 0:
        text_id = max(similarity_dict, key=similarity_dict.get)
        results.append(int(text_id))
        #print text_id
        similarity_dict.pop(text_id)
        
    return results
