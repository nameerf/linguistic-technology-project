import os
import time
from shutil import rmtree
from math import log10, sqrt

CLOSED_CLASS_CATEGORIES = ['CD', 'CC', 'DT', 'EX', 'IN', 'LS', 'MD', 'PDT', 'POS', 'PP', 
                           'PP$', 'RP', 'TO', 'UH', 'WDT', 'WP', 'WP$', 'WRB', 'SENT', "''"]
WORKING_DIRECTORY = os.getcwd()

#===============================================================================
# H synarthsh "create" dexetai ena orisma, to "number_of_files" to opoio 
# dhlwnei ton ari8mo twn arxeiwn poy apotelei thn syllogh mas. H synarthsh ayth
# einai h genikh synarthsh h opoia einai ypey8ynh gia thn dhmioyrgia twn
# eyretiriwn kai h opoia kalei tis synarthseis "create_initial_inverted_index",
# "set_lemmas_weights" kai "create_normal_index_from_inverted_index" gia to
# skopo ayto. Epishs edw ginetai kai h xronometrhsh ths diadikasia ths
# dhmiourgias twn dyo eyrethriwn. Telos h synarthsh epistrefei ta dyo eyrethria.
#===============================================================================
def create(number_of_files):
    inverted_index = dict()
    normal_index = dict()
    
    time_start = time.time()
    print 'Started creating indexes...'
    for i in range(number_of_files):
        create_initial_inverted_index(i, inverted_index)  

    weights_dict = set_lemmas_weights(inverted_index, number_of_files)
    
    for weights in inverted_index.values():
        for text_id in weights.keys():
            weights[text_id] /= weights_dict[text_id]
            
    normal_index = create_normal_index_from_inverted_index(inverted_index, normal_index)
    
    time_passed = time.time() - time_start
    print 'Finished creating indexes'
    print 'Time passed = {0} sec'.format(time_passed)
    
    temp_dir = os.path.join(WORKING_DIRECTORY,'tagged_files')
    rmtree(temp_dir)
    
    return (inverted_index, normal_index)

#===============================================================================
# H synarthsh "create_initial_inverted_index" dexetai dyo orismata: to 
# "text_id" pou einai to id tou pros epeksergasia keimenou kai to "lemma_dict"
# to opoio einai ena leksiko pou xrhsimopoieitai gia th dhmiourgia tou
# anestrammenou eyrethrioy. H synarthsh ayth leitourgei os ekshs: diabazei to
# etiketarismeno (tagged) arxeio (me id to "text_id") apo ton fakelo 
# "tagged_files" kai apo8hkeyei to arxeio grammh-grammh sth lista
# "all_lemmas_lines" (dhl. kai ta lhmmata kai ta merh tou logou pou anhkoun).
# Katopin kaleitai h synarthsh "extract_lemmas" me orisma thn lista
# "all_lemmas_lines" kai apo8hkeuontai sth lista "all_lemmas_list" ola ta
# lhmmata tou sygkekrimenou keimenou (symperilambanomenwn diplwn, triplwn klp.
# lhmmatwn). Sth synexeia ginetai mia arxikh dhmioyrgia toy anestrammenoy
# eyrethrioy (xoris na ginei o ypologismos twn barwn) to opoio apo8hkeytai
# telika sthn metablhth "lemma_dict".
#===============================================================================
def create_initial_inverted_index(text_id, lemma_dict):
    temp_dir = os.path.join(WORKING_DIRECTORY,'tagged_files')
    filename = temp_dir+'\\tagged_file_'+str(text_id)+'.txt'
    with open(filename, 'r') as f:
        all_lemmas_lines = f.readlines()
        
#    print all_lemmas_lines
    all_lemmas_list = extract_lemmas(all_lemmas_lines)

    total_lemmas = float(len(all_lemmas_list))
#    print total_lemmas
    
    for lemma in all_lemmas_list:
        if lemma in lemma_dict:
            nested_dict = lemma_dict[lemma]
            if text_id in nested_dict:
                counter = nested_dict[text_id]
                counter += 1/total_lemmas
                nested_dict[text_id] = counter
            else:
                nested_dict[text_id] = 1/total_lemmas
        else:
            lemma_dict[lemma] = {text_id:(1/total_lemmas)}

#===============================================================================
# H synarthsh "extract_lemmas" dexetai ena orisma, to "all_lemmas_lines" to
# opoio einai mia lista apo listes me tis esoterikes listes na periexoyn thn
# ka8e grammh toy (diabasmenoy etiketarismenoy) arxeioy (blepe thn synarthsh
# "create_initial_inverted_index"). H leitourgia ths synarthshs einai h ekshs:
# eksagontai apo th lista "all_lemmas_lines" mono ta lhmmata ta opoia den
# anhkoyn sthn kathgoria CLOSED_CLASS_CATEGORIES (opos exei oristei sthn arxh
# tou arxeioy, sthn kathgoria ayth periexontai ta stop-words klp.). Tyxon
# dipla, tripla klp. lhmmata mpainoyn kanonika sthn nea lista. Telos h nea
# lista "all_lemmas_list" epistrefetai.
#===============================================================================
def extract_lemmas(all_lemmas_lines):
    all_lemmas_list = []
    for i in range(len(all_lemmas_lines)):
        all_lemmas_lines[i] = all_lemmas_lines[i].split()
        nested_list = all_lemmas_lines[i]
        if nested_list[1] not in CLOSED_CLASS_CATEGORIES:
            all_lemmas_list.append(nested_list[2].lower())  # because tagger convert to uppercase
    return all_lemmas_list    

#===============================================================================
# H synarthsh ayth dexetai dyo orismata: to "lemma_dict" to opoio einai ena
# leksiko poy periexei mia proimh kai atelh ekdosh toy anestrammenoy eyrethrioy
# ka8os kai to "N" to opoio antiprosopeyei ton synoliko ari8mo twn arxeiwn ths
# sylloghs mas. H leitoyrgia ths sygkekrimenhs synarthshs einai apla o
# ypologismos twn barwn olwn twn lhmmatwn toy eyrethrioy symfona me ton
# algori8mo poy dinetai sthn ekfwnhsh ths askhshs. Ta barh ypologizontai kai
# apo8hkeyontai sth metablhth "weights_dict", h opoia einai ena leksiko me
# kleidia ta ids twn keimenwn kai times ta barh twn antistoixwn lhmmatwn gia to
# antistoixo keimeno. Telos to leksiko "weights_dict" epistrefetai.
#===============================================================================
def set_lemmas_weights(lemma_dict, N):
    weights_dict = dict()
    for i in range(N):
        weights_dict[i] = 0
    
    for weights in lemma_dict.values():
        n = len(weights)
        for text_id, tf in weights.iteritems():
            weights[text_id] = tf*(log10(float(N)/n))
            weights_dict[text_id] += pow(weights[text_id], 2)
    
    for i in range(N):
        weights_dict[i] = sqrt(weights_dict[i])
    
    #print '\n\n'
    #print weights_dict.items()
    
    return weights_dict

#===============================================================================
# H synarthsh "create_normal_index_from_inverted_index" dexetai dyo orismata:
# to "lemma_dict" sto opoio einai apo8hkeymeno to (oloklhrwmeno) anestrammeno
# eyrethrio kai to "text_dict" to opoio einai ena leksiko sto opoio 8a
# apo8hkeytei to kanoniko eyrethrio. H leitourgia ths synarthshs einai poly
# aplh: opws leei kai to onoma ths, dhmioyrgei to kanoniko eyrethrio apo to
# anestrammeno. Telos to kanoniko eyrethrio epistefetai.
#===============================================================================
def create_normal_index_from_inverted_index(lemma_dict, text_dict):
    for lemma, weights in lemma_dict.iteritems():
        for text_id, weight in weights.iteritems():
            if text_id not in text_dict:
                text_dict[text_id] = dict()
            nested_dict = text_dict[text_id]
            nested_dict[lemma] = weight
            
    return text_dict
