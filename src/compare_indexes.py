import time
import search_indexes as SE

#===============================================================================
# H synarthsh "compare_search_times" dexetai dyo orismata: ta "inverted_index"
# kai "normal_index", ta opoia einai to anestrammeno kai kanoniko eyrethrio
# antistoixa. H leitourgia ths einai h aksiologhsh kai sygkrish twn dyo
# eyrethriwn symfwna me tis odhgies ths ekfwnhshs. Dhladh gia ka8e eyrethrio
# ypoballontai ta erwthmata poy periexontai sto arxeio "queries.txt" kai
# ginontai oi antistoixes xronikes metrhseis. Logw tou oti oi metroumenoi xronoi
# einai mikroi trexoume tis metrhseis 10 fores metronata ton sunoliko xrono kai
# katopin diairoume me to 10 gia na paroume ton pragmatiko sta8mismeno xrono.
#===============================================================================
def compare_search_times(inverted_index, normal_index):
    with open('queries.txt', 'r') as f: 
        queries = f.readlines()
    #print queries
    num_of_queries = len(queries)
    
    total_time = 0
    total_time_per_query = 0
    for i in range(10):
        (time, time_per_query) = search_index("inverted", inverted_index, queries)
        total_time += time
        total_time_per_query += time_per_query
    print '\n- inverted index -'
    print 'time passed for a search of {0} queries = {1} sec'.format(num_of_queries, total_time/10)
    print 'time passed per query = {0} sec\n'.format(total_time_per_query/10)
    
    total_time = 0
    total_time_per_query = 0
    for i in range(10):
        (time, time_per_query) = search_index("normal", normal_index, queries)
        total_time += time
        total_time_per_query += time_per_query
    print '- normal index -'
    print 'time passed for a search of {0} queries = {1} sec'.format(num_of_queries, total_time/10)
    print 'time passed per query = {0} sec\n'.format(total_time_per_query/10)
    

#===============================================================================
# H synarthsh "search_index" dexetai tria orismata: ta "type_of_index", "index"
# kai "queries", ta opoia einai to eidos tou ekastote eyrethriou, to idio to
# eyrethrio kai ta erothmata antistoixa. H leitourgia ths einai apla na kalesei
# ka8e fore thn katallhlh sunarthsh anazhthshs basei tou ekastote eurethriou pou
# dinetai san orisma ka8os kai na xronometrhsei thn anazhthsh. Telika epistrefei
# ton xrono pou perase gia thn anazhthsh ka8os kai ton xrono ana erothma.
#===============================================================================
def search_index(type_of_index, index, queries):
    num_of_queries = len(queries)
    time_start = time.time()
    for query in queries:
        #print query.split()
        if(type_of_index == "inverted"):
            SE.search_inverted_index(index, query.split('\n'))
        else:
            SE.search_normal_index(index, query.split('\n'))
    time_passed = time.time() - time_start
    
    time_per_query = time_passed/num_of_queries
    
    return (time_passed, time_per_query)
    