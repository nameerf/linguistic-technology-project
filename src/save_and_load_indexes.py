import time
import xml.etree.cElementTree as ET

#===============================================================================
# H synarthsh "write_indexes_to_disk" dexetai dyo orismata: to "inverted_index"
# pou einai to anestrammeno eyrethrio kai to "normal_index" pou einai to
# kanoniko eyrethrio. H leitourgeia ths synarthshs einai apla h apo8hkeysh twn
# dyo eyrethriwn sto disko se or8a domhmena (well-formed) arxeia xml, me
# onomata "inverted_index.xml" kai "normal_index.xml" antistoixa (gia
# perissoteres leptomereies blepe anafora).
#===============================================================================
def write_indexes_to_disk(inverted_index, normal_index):
    time_start = time.time()
    print 'Writing indexes to disk...'
    
    with open('inverted_index.xml', 'w') as f:
        f.write('<inverted_index>\n')
        for lemma, weights in inverted_index.iteritems():
            f.write('\t<lemma name="'+lemma+'">\n')
            for text_id, weight in weights.iteritems():
                f.write('\t\t<document id="'+str(text_id)+'" weight="'+str(weight)+'"/>\n')
            f.write('\t</lemma>\n')
        f.write('</inverted_index>')
    
    with open('normal_index.xml', 'w') as f:
        f.write('<normal_index>\n')
        for text_id, lemmas in normal_index.iteritems():
            f.write('\t<document id="'+str(text_id)+'">\n')
            for lemma, weight in lemmas.iteritems():
                f.write('\t\t<lemma name="'+lemma+'" weight="'+str(weight)+'"/>\n')
            f.write('\t</document>\n')
        f.write('</normal_index>')
    
    time_passed = time.time() - time_start
    print 'Wrote indexes to disk'
    print 'Time passed = {0} sec'.format(time_passed)

#===============================================================================
# H synarthsh "load_indexes_from_disk" den dexetai kapoio orisma. H leitourgeia
# ths einai apla h fortwsh twn dyo eyrethriwn apo to sklhro disko sth mnhmh.
# Epistrfei ta dyo eyrethria, afou prwta ta apo8hkeysei se antistoixa katallhla
# leksika.
#===============================================================================
def load_indexes_from_disk():
    inverted_index = dict() 
    normal_index = dict()
    
    for event, elem in ET.iterparse("inverted_index.xml"):
        if elem.tag == 'lemma':
            lemma = elem.get('name')
            inverted_index[lemma] = dict()
            for child in list(elem):
                text_id = int(child.get('id'))
                weight = float(child.get('weight'))
                inverted_index[lemma][text_id] = weight
                child.clear()
            elem.clear()
    
    for event, elem in ET.iterparse("normal_index.xml"):
        if elem.tag == 'document':
            text_id = int(elem.get('id'))
            normal_index[text_id] = dict()
            for child in list(elem):
                lemma = child.get('name')
                weight = float(child.get('weight'))
                normal_index[text_id][lemma] = weight
                child.clear()
            elem.clear()
    
#    with open('inverted_index_lemmas.txt', 'w') as f:
#        for lemma in inverted_index.iterkeys():
#            f.write(lemma+'\n')
#            
#    with open('normal_index_lemmas.txt', 'w') as f:
#        for weights in normal_index.itervalues():
#            for lemma in weights.iterkeys():
#                f.write(lemma+'\n')
    
    return (inverted_index, normal_index)
